﻿using System.Data.SqlClient;
using System.Reflection;
using System.Text;


Console.Write("Generator SQL script for Module Nawadata Framework (version 0.1)");
Console.WriteLine();

Run();
Console.Read();

void Run()
{
    Console.WriteLine("");
    Console.Write("Input Server name: ");
    var server = Console.ReadLine();
    if (string.IsNullOrEmpty(server))
    {
        Console.Write("Server name cannot be empty");
        Environment.Exit(0);
    }

    Console.Write("Input Username: ");
    var username = Console.ReadLine();
    if (string.IsNullOrEmpty(username))
    {
        Console.Write("Username cannot be empty");
        Environment.Exit(0);
    }

    Console.Write("Input Password: ");
    var password = "";
    ConsoleKey key;
    do
    {
        var keyInfo = Console.ReadKey(intercept: true);
        key = keyInfo.Key;

        if (key == ConsoleKey.Backspace && password.Length > 0)
        {
            Console.Write("\b \b");
            password = password[0..^1];
        }
        else if (!char.IsControl(keyInfo.KeyChar))
        {
            Console.Write("*");
            password += keyInfo.KeyChar;
        }
    } while (key != ConsoleKey.Enter);
    Console.WriteLine();
    if (string.IsNullOrEmpty(password))
    {
        Console.Write("Password cannot be empty");
        Environment.Exit(0);
    }

    Console.Write("Input Database/Scheme: ");
    var database = Console.ReadLine();
    if (string.IsNullOrEmpty(database))
    {
        Console.Write("Database cannot be empty");
        Environment.Exit(0);
    }

    Console.WriteLine("Getting Connection ...");
    //your connection string 
    string connString = @"Data Source=" + server + ";Initial Catalog="
                + database + ";Persist Security Info=True;User ID=" + username + ";Password=" + password;

    //create instanace of database connection
    SqlConnection conn = new SqlConnection(connString);
    try
    {
        Console.WriteLine("Opening Connection ...");
        //open connection
        conn.Open();
        GenerateOneFile(conn);
        conn.Close();
    }
    catch (Exception e)
    {
        conn.Close();
        Console.WriteLine("Error: " + e.Message);
    }

}

void GenerateOneFile(SqlConnection conn)
{
    Console.Write("Input PK Module ID: ");
    var pkModuleId = Console.ReadLine();
    if (string.IsNullOrEmpty(pkModuleId))
    {
        Console.Write("PK Module ID cannot be empty");
        Environment.Exit(0);
    }

    var moduleName = "";
    using (SqlCommand command = new SqlCommand("SELECT * FROM Module where PK_Module_ID = " + pkModuleId, conn))
    {
        using (SqlDataReader reader = command.ExecuteReader())
        {
            while (reader.Read())
            {
                for (int i = 0; i < 2; i++)
                {
                    moduleName = reader.GetValue(i).ToString() ?? "";
                }
                Console.WriteLine();
            }
        }
    }

    string fileName = pkModuleId + " - " + moduleName + ".sql";
    Console.WriteLine("Generating sql " + fileName);

    var res1 = GenerateSqlModuleStringByPkModuleId(pkModuleId, moduleName, conn);

    //string basePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
    string basePath = Directory.GetCurrentDirectory();
    string pathString = System.IO.Path.Combine(basePath, "Query Module");
    System.IO.Directory.CreateDirectory(pathString);

    var filePath = System.IO.Path.Combine(pathString, fileName);

    FileStream filestream = new FileStream(filePath, FileMode.Create, FileAccess.Write);
    using (StreamWriter writer = new StreamWriter(filestream))
    {
        writer.Write(res1);
        writer.AutoFlush = true;
    }

    Console.WriteLine("Success generate file : " + fileName);

    Console.WriteLine("Are you want to generate other file ? Press y. To exit press other than y");

    var keyInfo = Console.ReadKey(intercept: true);
    if (keyInfo.Key == ConsoleKey.Y)
    {
        Console.WriteLine();
        GenerateOneFile(conn);
    }
    else
    {
        conn.Close();
        Environment.Exit(0);
    }
}

string GenerateSqlModuleStringByPkModuleId(string pkModuleId, string moduleName, SqlConnection conn)
{
    string template = @"--script untuk table {{ModuleName}}
BEGIN TRANSACTION Trans1

BEGIN TRY
DECLARE @PK_IDENTITY_INSERT_Module BIGINT = {{PK_Module_ID}}

--pkmodule variable
DECLARE @pkmodule BIGINT
--get pk module
SELECT @pkmodule = PK_Module_ID FROM dbo.Module WHERE ModuleName = '{{ModuleName}}'

--check if module {{ModuleName}} is exist
IF EXISTS (SELECT * FROM dbo.Module WHERE ModuleName = '{{ModuleName}}')
BEGIN
    --check modulefield exists
    IF EXISTS (SELECT * FROM dbo.ModuleField WHERE FK_Module_ID = @pkmodule)
    BEGIN

        --delete modulefield first
        PRINT ('Deleting ModuleField...')

        DELETE FROM dbo.ModuleField WHERE FK_Module_ID = @pkmodule

        PRINT ('Deleting ModuleField Success.' + CHAR(10))

    END

    --check modulevalidation
    IF EXISTS(SELECT * FROM dbo.ModuleValidation WHERE FK_Module_ID = @pkmodule)
    BEGIN

        --delete modulevalidation first
        PRINT ('Deleting ModuleValidation...')

        DELETE FROM dbo.ModuleValidation WHERE FK_Module_ID = @pkmodule

        PRINT ('Deleting ModuleValidation Success.' + CHAR(10))

    END

{{tableExist}}
    --delete module {{ModuleName}} 
    PRINT ('Deleting Module {{ModuleName}}...')

    DELETE FROM dbo.Module WHERE PK_Module_ID = @pkmodule

    PRINT ('Deleting Module {{ModuleName}} Success.' + CHAR(10))

END


---------------------------------------------------------------------------------------------------------------------------------------------


BEGIN

--insert record module
PRINT ('Inserting Module...')

SET IDENTITY_INSERT dbo.Module ON
INSERT INTO dbo.Module
(
    PK_Module_ID,
    ModuleName,
    ModuleLabel,
    ModuleDescription,
    IsUseDesigner,
    IsUseApproval,
    IsSupportAdd,
    IsSupportEdit,
    IsSupportDelete,
    IsSupportActivation,
    IsSupportView,
    IsSupportUpload,
    IsSupportDetail,
    UrlAdd,
    UrlEdit,
    UrlDelete,
    UrlActivation,
    UrlView,
    UrlUpload,
    UrlApproval,
    UrlApprovalDetail,
    UrlDetail,
    IsUseStoreProcedureValidation,
    Active,
    CreatedBy,
    LastUpdateBy,
    ApprovedBy,
    CreatedDate,
    LastUpdateDate,
    ApprovedDate
)
VALUES
(   
    {{listColumnModule}}
)

SET IDENTITY_INSERT dbo.Module OFF

PRINT ('Inserting Module Success.' + CHAR(10))

END

---------------------------------------------------------------------------------------------------------------------------------------------
--get pk module
SELECT @pkmodule = PK_Module_ID FROM dbo.Module WHERE ModuleName = '{{ModuleName}}'

---------------------------------------------------------------------------------------------------------------------------------------------

";
    var templateTableExist = @"--check if table exist then drop table first
    IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{{ModuleName}}')
    BEGIN

        --drop table
        PRINT ('Dropping Table {{ModuleName}}...')

        DROP TABLE dbo.{{ModuleName}}
        DROP TABLE dbo.{{ModuleName}}_Upload

        PRINT ('Dropping Table {{ModuleName}} Success.' + CHAR(10))

    END";

    var templateSupportModuleField = @"--insert ulang modulefield
PRINT ('Inserting ModuleField...')

INSERT INTO dbo.ModuleField
(
    FK_Module_ID,
    FieldName,
    FieldLabel,
    [Sequence],
    [Required],
    IsPrimaryKey,
    IsUnik,
    IsShowInView,
    IsShowInForm,
    DefaultValue,
    FK_FieldType_ID,
    SizeField,
    FK_ExtType_ID,
    TabelReferenceName,
    TabelReferenceNameAlias,
    TableReferenceFieldKey,
    TableReferenceFieldDisplayName,
    TableReferenceFilter,
    IsUseRegexValidation,
    TableReferenceAdditonalJoin,
    BCasCade,
    FieldNameParent,
    FilterCascade
)
VALUES
{{listsModuleField}}    

PRINT ('Insert ModuleField Success.' + CHAR(10))

---------------------------------------------------------------------------------------------------------------------------------------------
";

    var templateSupportSp = @"--insert ulang modulevalidation
PRINT ('Inserting ModuleValidation...')

INSERT INTO ModuleValidation
(
    FK_Module_ID,
    FK_ModuleAction_ID,
    FK_ModuleTime_ID,
    StoreProcedureName,
    StoreProcedureParameter,
    StoreProcedureParameterValueFieldSequence
)
VALUES
{{listsModuleValidation}}    


PRINT ('Insert ModuleValidation Success.' + CHAR(10))

---------------------------------------------------------------------------------------------------------------------------------------------

 ";

    var templateExec = @"--create ulang table
PRINT ('Create Table {{ModuleName}}...')

EXEC dbo.usp_generateTable @PkModuleID = @pkmodule -- int
EXEC dbo.usp_GenerateTableUpload @PkModuleID = @pkmodule -- int

PRINT ('Create Table {{ModuleName}} Success.' + CHAR(10))

";

    var templateEnd = @"COMMIT TRANSACTION Trans1

END TRY

BEGIN CATCH

    ROLLBACK TRANSACTION Trans1

END CATCH";

    var stringBuilder = new StringBuilder(template);
    var isSupportModuleDesigner = false;
    var isUseStoreProcedureValidation = false;

    // Column Module 
    var stringBuilderModule = new StringBuilder();
    using (SqlCommand command = new SqlCommand("SELECT * FROM Module where PK_Module_ID = " + pkModuleId, conn))
    {
        using (SqlDataReader reader = command.ExecuteReader())
        {
            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    var itemColumn = reader.GetValue(i).ToString();
                    if (string.IsNullOrEmpty(itemColumn))
                    {
                        itemColumn = "''";
                    }
                    else if (itemColumn == "True")
                    {
                        itemColumn = "1";
                    }
                    else if (itemColumn == "False")
                    {
                        itemColumn = "0";
                    }
                    else
                    {
                        itemColumn = "'" + itemColumn + "'";
                    }

                    var stringColumn = "    " + itemColumn;
                    if (i == 0)
                    {
                        stringBuilderModule.Append("@PK_IDENTITY_INSERT_Module," + "\n");
                    }
                    else if (i == 4)
                    {
                        if (itemColumn == "1")
                        {
                            isSupportModuleDesigner = true;
                        }
                        else
                        {
                            isSupportModuleDesigner = false;
                        }

                    }
                    else if (i == 22)
                    {
                        if (itemColumn == "1")
                        {
                            isUseStoreProcedureValidation = true;
                        }
                        else
                        {
                            isUseStoreProcedureValidation = false;
                        }

                    }
                    else if (i == reader.FieldCount - 1)
                    {
                        stringBuilderModule.Append("    GETDATE()" + "\n");
                    }

                    else if (i > reader.FieldCount - 4)
                    {
                        stringBuilderModule.Append("    GETDATE()," + "\n");
                    }
                    else
                    {
                        stringBuilderModule.Append(stringColumn + ",\n");
                    }
                }
            }
        }
    }

    if (isSupportModuleDesigner)
    {
        stringBuilder.Append(templateSupportModuleField);
        stringBuilder.Replace("{{tableExist}}", templateTableExist);
    }
    else
    {
        stringBuilder.Replace("{{tableExist}}", "");
    }

    if (isUseStoreProcedureValidation)
    {
        stringBuilder.Append(templateSupportSp);
    }

    if (isSupportModuleDesigner)
    {

        stringBuilder.Append(templateExec);
    }

    stringBuilder.Append(templateEnd);


    stringBuilder.Replace("{{ModuleName}}", moduleName);
    stringBuilder.Replace("{{PK_Module_ID}}", pkModuleId.ToString());
    stringBuilder.Replace("{{listColumnModule}}", stringBuilderModule.ToString());

    if (isSupportModuleDesigner)
    {
        var listStringModuleField = new List<String>();
        var stringBuilderModuleField = new StringBuilder();
        using (SqlCommand command = new SqlCommand("select * from ModuleField where FK_Module_ID = " + pkModuleId, conn))
        {
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var stringBuilderModuleFieldItem = new StringBuilder("( ");
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        var itemColumn = reader.GetValue(i).ToString();
                        if (string.IsNullOrEmpty(itemColumn))
                        {
                            itemColumn = "''";
                        }
                        else if (itemColumn == "True")
                        {
                            itemColumn = "1";
                        }
                        else if (itemColumn == "False")
                        {
                            itemColumn = "0";
                        }
                        else
                        {
                            itemColumn = "'" + itemColumn + "'";
                        }

                        var stringColumn = itemColumn;
                        if (i == 0)
                        {
                            stringBuilderModuleFieldItem.Append("@pkmodule, ");
                        }
                        else if (i == reader.FieldCount - 1)
                        {
                            stringBuilderModuleFieldItem.Append(stringColumn);
                        }

                        else
                        {
                            stringBuilderModuleFieldItem.Append(stringColumn + ", ");
                        }
                    }
                    stringBuilderModuleFieldItem.Append(" )");
                    listStringModuleField.Add(stringBuilderModuleFieldItem.ToString());
                }
            }
        }

        for (int i = 0; i < listStringModuleField.Count; i++)
        {
            if (i == listStringModuleField.Count - 1)
            {
                stringBuilderModuleField.Append(listStringModuleField[i] + "\n");
            }
            else
            {
                stringBuilderModuleField.Append(listStringModuleField[i] + ", \n");
            }
        }

        stringBuilder.Replace("{{listsModuleField}}", stringBuilderModuleField.ToString());
    }

    if (isUseStoreProcedureValidation)
    {
        var listStringModuleField = new List<String>();
        var stringBuilderModuleField = new StringBuilder();
        using (SqlCommand command = new SqlCommand("select * from ModuleValidation where FK_Module_ID = " + pkModuleId, conn))
        {
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var stringBuilderModuleFieldItem = new StringBuilder("( ");
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        var itemColumn = reader.GetValue(i).ToString();
                        if (string.IsNullOrEmpty(itemColumn))
                        {
                            itemColumn = "''";
                        }
                        else if (itemColumn == "True")
                        {
                            itemColumn = "1";
                        }
                        else if (itemColumn == "False")
                        {
                            itemColumn = "0";
                        }
                        else
                        {
                            itemColumn = "'" + itemColumn + "'";
                        }

                        var stringColumn = itemColumn;
                        if (i == 0)
                        {
                            stringBuilderModuleFieldItem.Append("@pkmodule, ");
                        }
                        else if (i == reader.FieldCount - 1)
                        {
                            stringBuilderModuleFieldItem.Append(stringColumn);
                        }

                        else
                        {
                            stringBuilderModuleFieldItem.Append(stringColumn + ", ");
                        }
                    }
                    stringBuilderModuleFieldItem.Append(" )");
                    listStringModuleField.Add(stringBuilderModuleFieldItem.ToString());
                }
            }
        }

        for (int i = 0; i < listStringModuleField.Count; i++)
        {
            if (i == listStringModuleField.Count - 1)
            {
                stringBuilderModuleField.Append(listStringModuleField[i] + "\n");
            }
            else
            {
                stringBuilderModuleField.Append(listStringModuleField[i] + ", \n");
            }
        }

        stringBuilder.Replace("{{listsModuleValidation}}", stringBuilderModuleField.ToString());
    }

    var result = stringBuilder.ToString();
    return result;
}
